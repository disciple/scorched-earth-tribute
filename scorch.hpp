#ifndef SCORCH_HPP
#define SCORCH_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include <iostream>
#include "graphics.hpp"
#include "entities.hpp"
#include "gameLogic.hpp"
#include "input.hpp"
class Scorch_c
{
	public:
	// ---- values
	int x;
	// ---- functions
	Scorch_c();
	~Scorch_c();

	Entities_c Entities;
	Graphics_c Graphics;
	GameLogic_c GameLogic;

	void mainLoop();
};
#endif
