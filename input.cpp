#include "input.hpp"
// -------- START CLASS INPUT
Input_c::Input_c():
	left(false),
	right(false),
	down(false),
	up(false),
	cursorX(0),
	cursorY(0)
{
	std::cout<<"Initializing Input_c\n";
	// ---- set the global inputCallbackData to point to Input's variables
	inputPointer = this;
	/*
	inputCallbackData.mouseClicks = &mouseClicks;
	inputCallbackData.x = &cursorX;
	inputCallbackData.y = &cursorY;
	inputCallbackData.left = &left;
	inputCallbackData.right = &right;
	inputCallbackData.down = &down;
	inputCallbackData.up = &up;
	*/
	// ---- END set
	// ASSUMING glfwInit() has been called (called in Render_c initializer)
	// set GLFW callbacks
	glfwSetKeyCallback(*globalGraphicsContext.glfwWindow, glfwKeyboardCallback);
	glfwSetMouseButtonCallback(*globalGraphicsContext.glfwWindow, glfwMouseButtonCallback);
}

Input_c::~Input_c()
{
	std::cout<<"Deleting Input_c\n";
}

void Input_c::updateCursor()
{
	double x;
	double y;
	glfwGetCursorPos(*globalGraphicsContext.glfwWindow, &x, &y);
//	std::cout<<"x = "<<x<<", y = "<<y<<"\n";
	cursorX = (int) x;
	cursorY = (int) y;
//	std::cout<<"cursorX = "<<cursorX<<", cursorY = "<<cursorY<<"\n";
}

void Input_c::updateVirtCursor(double &virtCursorX, double &virtCursorY)
{
	updateCursor();
	virtCursorX = ((cursorX / (double) *globalGraphicsContext.windowW) * 
			*globalGraphicsContext.virtScreenW);

	virtCursorY = (double) ((cursorY / (double) *globalGraphicsContext.windowH) * 
			*globalGraphicsContext.virtScreenH);
	virtCursorY = (*globalGraphicsContext.virtScreenH - 1) - virtCursorY;
	virtCursorY += 0.5;
//	std::cout<<"virtCursorX = "<<virtCursorX<<" virtCursorY = "<<virtCursorY<<"\n";
//	std::cout<<"targetVirtPixX = "<<targetVirtPixX<<" targetVirtPixY = "<<targetVirtPixY<<"\n";
}

// -------- END CLASS INPUT

void glfwKeyboardCallback(GLFWwindow *window, int key, int scancode, int action, int mods)
{
	if(action == GLFW_PRESS)
	{
		std::cout<<"Pressed a keyboard button\n";
		/*
		if(key == GLFW_KEY_A){
			std::cout<<"pressed A, entering ("<<gameState.curVirtX<<", "<<gameState.curVirtY<<")\n";
			modifyDirt_t dirt = {ADD_DIRT, gameState.curVirtX, gameState.curVirtY, 50};
			gameState.modifyDirt_q.push(dirt);
		}
		*/
	}
}

void glfwMouseButtonCallback(GLFWwindow *window, int button, int action, int mods)
{
	double x;
	double y;
	inputPointer->updateVirtCursor(x, y);
	mouseClick_t mouseClick;
	mouseClick.x = x;
	mouseClick.y = y;
	std::cout<<"callback x: "<<mouseClick.x<<", callback y: "<<mouseClick.y<<"\n";
	if(action == GLFW_PRESS)
	{
		if(button == GLFW_MOUSE_BUTTON_1)
		{
			mouseClick.clickType = LMB_PRESS;
		}
		else if(button == GLFW_MOUSE_BUTTON_2)
		{
			mouseClick.clickType = RMB_PRESS;
		}
	}
	else if(action == GLFW_RELEASE)
	{
		if(button == GLFW_MOUSE_BUTTON_1)
		{
			mouseClick.clickType = LMB_RELEASE;
		}
		else if(button == GLFW_MOUSE_BUTTON_2)
		{
			mouseClick.clickType = RMB_RELEASE;
		}
	}
	(inputPointer->mouseClicks).push(mouseClick);	
}
