#include "entities.hpp"
#include "GL/glew.h"
#include "GLFW/glfw3.h"
const double PI = 3.141592654;

Entities_c * EntitiesPointer;
// -------- START CLASS PROJECTILE
Projectile_c::Projectile_c()
{
	std::cout<<"Initializing Projectile_c\n";
}

Projectile_c::~Projectile_c()
{
	std::cout<<"Deleting Projectile_c\n";
}

void Projectile_c::advance(double timeDisp)
{
	xLoc += xVelocity;
	yLoc += yVelocity;
//	std::cout<<"new xLoc = "<<xLoc<<"\n";
}

completionStatus_e Projectile_c::checkForEvents()
{
	completionStatus_e retVal = NOT_DONE;
	if(distance2d(xOrigin, yOrigin, xLoc, yLoc) > 100)
	{
		EntitiesPointer->mutateDirt((int) xLoc, (int) yLoc, 10.0, Entities_c::ADD);
		retVal = DONE;
	}
	return retVal;
}

void Projectile_c::conventionalExplosion(int xPix, int yPix, int radius)
{
}
// -------- END CLASS PROJECTILE

// -------- START CLASS TANK
Tank_c::Tank_c():
	traction(false),
	tankType(STANDARD),
	health(100.0f),
	currentWeapon(STANDARD_ROUND),
	gunRotation(0.0),
	gunHeight(2.5),
	maxPower(100.0f),
	x(0),
	y(0),
	accuX(x),
	accuY(y),
	accuZ(1.0)
{
	std::cout<<"Initializing Tank_c\n";
	for(int i = 0; i < MAX_WEAPON_COUNT; ++i)
	{
		weapons[i] = 0;
	}
}

Tank_c::~Tank_c()
{
	std::cout<<"Deleting Tank_c\n";
}

void Tank_c::updateGunRotation(double virtCursorX, double virtCursorY)
{
	GLfloat deltaX = virtCursorX - x;
	GLfloat deltaY = virtCursorY - (y + gunHeight);
	gunRotation = atan2(deltaY, deltaX) * (180 / PI);
}

void Tank_c::displaceCoords(double xDisp, double yDisp)
{
	accuX += xDisp;
	accuY += yDisp;
	x = (int) accuX;
	y = (int) accuY;
}

void Tank_c::setCoords(double x, double y)
{
	accuX = x;
	accuY = y;
	x = (int) accuX;
	y = (int) accuY;
}

void Tank_c::fireProjectile(weaponType_e weaponType)
{
	GLfloat muzzleVelocity = 1.0;
	vectorf_t gunTip;
	vectorf_t gunPivot = {accuX, accuY + (GLfloat) TANK_GUN_Y_DISP, accuZ};
	gunTip.x = accuX + TANK_GUN_LENGTH;
	gunTip.y = accuY + TANK_GUN_Y_DISP;
	gunTip.z = accuZ;
	rotateVectfArbXY(gunTip, gunRotation, gunPivot.x, gunPivot.y);
			
	//	TODO remove
//	std::cout<<"gunRotation = "<<gunRotation<<"\n";
//	std::cout<<"gunTipX = "<<gunTip.x<<" gunTipY = "<<gunTip.y<<"\n";
	//TODO convert to state current weapon selection
	Projectiles.emplace_back();
	Projectiles.back().xOrigin = gunTip.x;
	Projectiles.back().yOrigin = gunTip.y;
	std::cout<<"xOrigin = "<<Projectiles.back().xOrigin<<" yOrigin = "<<Projectiles.back().yOrigin<<"\n";
	Projectiles.back().xLoc = gunTip.x;
	Projectiles.back().yLoc = gunTip.y;
	// first bring to origin then divide by gun length, then multiply by muzzleVelocity to get accurate vectors for the target initial speed
	Projectiles.back().xVelocity = ((gunTip.x - accuX) / (GLfloat) TANK_GUN_LENGTH) 
		* muzzleVelocity;
	Projectiles.back().yVelocity = ((gunTip.y - accuY - TANK_GUN_Y_DISP) / (GLfloat) TANK_GUN_LENGTH) 
		* muzzleVelocity;
	std::cout<<"Projectiles size = "<<Projectiles.size()<<"\n";;
	//TODO somewhere when checking for projectile hit use the fact that the projectiles are in the Tank class to allocate points based on what tank fired them
}

void Tank_c::updateProjectileStatus(double timeDisp)
{
	int projectileCount = Projectiles.size();
	for(int i = projectileCount - 1; i >= 0; --i)
	{
		if(Projectiles[i].checkForEvents() == DONE)
		{
			Projectiles.erase(Projectiles.begin() + i);
		}
	}
}

// -------- END CLASS TANK

// -------- START CLASS ENTITIES
Entities_c::Entities_c():
	allTanksHaveTraction(false),
	// mountain
	virtualPixelCountW(720),
	virtualPixelCountH(405),
	virtualPixelCount(virtualPixelCountW * virtualPixelCountH),
	virtPixDropComplete(false)
{
	std::cout<<"Initializing Entities_c\n";
	//point activeEntitiesPointer to this class
	EntitiesPointer = this;
	
	// Projectiles
	Projectiles.emplace_back();

	// Tanks
	// add first tank
	Tanks.emplace_back();
	Tanks.back().displaceCoords(200.0f, 200.0f);

	// mountain
	virtualPixels = new bool[virtualPixelCount];
	for(int i = 0; i < virtualPixelCount; ++i)
	{
		//TODO remove when creating more details maps
		if(i < virtualPixelCountW)
		{
			virtualPixels[i] = true;
		}
		else
		{
			virtualPixels[i] = false;
		}
	}
}

Entities_c::~Entities_c()
{
	std::cout<<"Deleting Entities_c\n";
	delete[] virtualPixels;
}

void Entities_c::applyVirtCursor(double virtCursorX, double virtCursorY)
{
	int tankCount = Tanks.size();
	for(int i = 0; i < tankCount; ++i)
	{
		Tanks[i].updateGunRotation(virtCursorX, virtCursorY);
	}
}

void Entities_c::updateState_Time(double timeDisp)
{
	updateTanks(timeDisp);
}

// ---- virtual pixels
int Entities_c::getColumnHeight(int index)
{
	int retVal = 0;
	if(index < 0 || index > virtualPixelCountW - 1)
	{
		std::cout<<"ERROR: calling Entities_c::getColumnHeight() with out-of-bounds column index\n";
	}
	for(int i = index; i < virtualPixelCount; i += virtualPixelCountW)
	{
		if(!virtualPixels[i])
		{
			retVal = i / virtualPixelCountW;
			break;
		}
	}
	return retVal;
}

void Entities_c::mutateDirt(int x, int y, int radius, mutateType_e mutateType)
{
	// reset boolean to make game recheck for virtPixels to drop
	virtPixDropComplete = false;
	
	// set range to add dirt
	int startX = x - radius;
	int endX = x + radius;
	int startY = y - radius;
	int endY = y + radius;
	// cap ranges to fit within virtual pixel range
	if(startX < 0) startX = 0;
	if(endX > virtualPixelCountW) endX = virtualPixelCountW;
	if(startY < 0) startY = 0;
	if(endY > virtualPixelCountH) endY = virtualPixelCountH;
	bool value;
	if(mutateType == ADD)
	{
		value = true;
	}
	else
	{
		value = false;
	}
	for(int i = startX; i < endX; ++i){
		for(int j = startY; j < endY; ++j){
			int radicand = (i - x) * (i - x) + (j - y) * (j - y);
			if(radicand <= 0){
				radicand = 1;
			}
			double d = sqrt(radicand);
			if(d <= radius){
				virtualPixels[(j * virtualPixelCountW) + i] = value;
			}
		}
	}
}

void Entities_c::dropVirtPixels(double time)
{
	bool* vPix = virtualPixels;
	int vPixW = virtualPixelCountW;
	int pixelsMoved = 0;
	// virtual pixels represented as starting at lower left of map, so iteration starts at bottom row and goes up
	for(int i = 0; i < virtualPixelCount; ++i)
	{
		//if i is not on the bottom row
		int y = (int) i / (int) vPixW;	
		if(y > 0){
			// if i is a virtualPixel and there's a free spot below, move it down
			if(vPix[i] && !vPix[i - vPixW])
			{
				++pixelsMoved;
				vPix[i - vPixW] = true;
				vPix[i] = false;
			}
		}
	}
	if(pixelsMoved == 0)
	{
		virtPixDropComplete = true;
	}
}

// ---- entity/Virtual pixel interaction
void Entities_c::dropTanks(double timeDisp)
{
	int tankCount = Tanks.size();
	for(int i = 0; i < tankCount; ++i)
	{
		Tanks[i].displaceCoords(0.0, -20.0 * timeDisp);
		int x = Tanks[i].x;
		int y = Tanks[i].y;
		GLfloat accuX = Tanks[i].accuX;
		GLfloat accuY = Tanks[i].accuY;
		//if tank is colliding with any mountain stop it. Looping through tank width
		int columnHeight = 0;
		// check across tank to make sure the heighest column represents columnHeight
		int min = bound(x - 4, 0, virtualPixelCountW);
		int max = bound(x + 4, 0, virtualPixelCountW);
		for(int i = min; i < max; ++i)
		{
			int testColumn = getColumnHeight(i);
			if(testColumn > columnHeight)
			{
				columnHeight = testColumn;
			}
		}
	//	std::cout<<"columnHeight for column index = "<<x<<" is "<<columnHeight<<"\n";
		if(y < columnHeight)
		{
			Tanks[i].setCoords(accuX, columnHeight);
		}
		//keep tank from falling below 0
		if(accuY < 0.0)
		{
			Tanks[i].setCoords(accuX, 0.0);
		}
	//	std::cout<<"x = "<<x<<" and "<<accuX<<", y = "<<y<<" and "<<accuY<<"\n";
	}
}

void Entities_c::advanceProjectiles(double timeDisp)
{
	int tankCount = Tanks.size();
	// 
	for(int i = 0; i < tankCount; ++i)
	{
		int projectileCount = Tanks[i].Projectiles.size();
		for(int j = 0; j < projectileCount; ++j)
		{
			Tanks[i].Projectiles[j].advance(timeDisp);
		}
	}
}

void Entities_c::updateProjectileStatuses(double timeDisp)
{
	int tankCount = Tanks.size();
	for(int i = 0; i < tankCount; ++i)
	{
		Tanks[i].updateProjectileStatus(timeDisp);
	}
}

void Entities_c::updateTanks(double timeDisp)
{
	dropTanks(timeDisp);
	advanceProjectiles(timeDisp);
	updateProjectileStatuses(timeDisp);
}
// -------- END CLASS ENTITIES
