#ifndef INPUT_HPP
#define INPUT_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "graphics.hpp"
#include <iostream>
#include <queue>

enum clickType_e
{
	LMB_PRESS,
	LMB_RELEASE,
	RMB_PRESS,
	RMB_RELEASE
};

struct mouseClick_t
{
	clickType_e clickType;
	int x;
	int y;
};

// global for edge cases like GLFW callbacks which don't work with C++ class methods
/*
struct inputCallbackData_t
{
	std::queue<mouseClick_t> * mouseClicks;
	int * x;
	int * y;
	bool * left;
	bool * right;
	bool * down;
	bool * up;
};
static inputCallbackData_t inputCallbackData;
*/

class Input_c
{
	public:
	// ---- values
	bool left;
	bool right;
	bool down;
	bool up;
	int cursorX;
	int cursorY;
	std::queue<mouseClick_t> mouseClicks;
	// ---- functions
	Input_c();
	~Input_c();
	void updateCursor();
	void updateVirtCursor(double &virtCursorX, double &virtCursorY);
};
static Input_c * inputPointer;

void glfwKeyboardCallback(GLFWwindow *window, int key, int scancode, int action, int mods);
void glfwMouseButtonCallback(GLFWwindow *window, int button, int action, int mods);

#endif
