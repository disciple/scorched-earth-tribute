#ifndef UTILITY_HPP
#define UTILITY_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "cmath"
#include <iostream>
struct vectorf_t
{
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

int bound(int num, int min, int max);

// rotates vectorf_t around arbitrary x y z coordinates
void rotateVectfArbXY(vectorf_t &vector, GLfloat angle, GLfloat x, GLfloat y);

GLfloat distance2d(GLfloat x0, GLfloat y0, GLfloat x1, GLfloat y1);
#endif
