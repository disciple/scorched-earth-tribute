#include "gameLogic.hpp"
// -------- START CLASS GRAPHICS
GameLogic_c::GameLogic_c():
	gameState(PLAYER_TURN),
	targetVirtPixX(0),
	targetVirtPixY(0),
	virtCursorX(0),
	virtCursorY(0)
{
	std::cout<<"Initializing GameLogic_c\n";
	std::cout<<"*globalGraphicsContext.windowW = "<<*globalGraphicsContext.windowW<<"\n";
}

GameLogic_c::~GameLogic_c()
{
	std::cout<<"Deleting GameLogic_c\n";
}

void GameLogic_c::update()
{
	static double lastTime = 0;
	static double timeDisp = 0;
	timeDisp = glfwGetTime() - lastTime;
	// handle all mouse/keyboard input
	processInput();

	updateTargetVirtPix();
	// update entities with relevant data
	EntitiesPointer->applyVirtCursor(virtCursorX, virtCursorY);
		
	// time-based updates
	// entity updates
	if(timeDisp > TIME_SLICE)
	{
		lastTime = glfwGetTime();
		EntitiesPointer->updateState_Time(TIME_SLICE);

		// drop virtPixs if drop is not complete
		if(! EntitiesPointer->virtPixDropComplete)
		{
			EntitiesPointer->dropVirtPixels(TIME_SLICE);
		}
	}
}

void GameLogic_c::updateTargetVirtPix()
{
	Input.updateVirtCursor(virtCursorX, virtCursorY);
	targetVirtPixX = (int) virtCursorX;
	targetVirtPixY = (int) virtCursorY;
}

void GameLogic_c::processInput()
{
	// handle mouse input
	int mouseQueueSize = Input.mouseClicks.size();
	for(int i = 0; i < mouseQueueSize; ++i)
	{
		mouseClick_t click = Input.mouseClicks.front();
		if(click.clickType == LMB_PRESS)
		{
			EntitiesPointer->mutateDirt(click.x, click.y, 10.0, Entities_c::ADD);
			EntitiesPointer->allTanksHaveTraction = false;
			EntitiesPointer->Tanks.back().fireProjectile(STANDARD_ROUND);
		}
		if(click.clickType == RMB_PRESS)
		{
			//EntitiesPointer->mutateDirt(click.x, click.y, 10.0, REMOVE);
			EntitiesPointer->mutateDirt(click.x, click.y, 10.0, Entities_c::REMOVE);
			EntitiesPointer->allTanksHaveTraction = false;
		}
		Input.mouseClicks.pop();
	}
}
// -------- END CLASS GRAPHICS
