#ifndef GRAPHICS_HPP
#define GRAPHICS_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "utility.hpp"
#include "vmath.h"
#include <iostream>
#include <string>

// global for edge cases like GLFW callbacks which don't work with C++ class methods
struct globalGraphicsContext_t
{
	GLFWwindow ** glfwWindow;
	int * windowW;
	int * windowH;
	int * virtScreenW;
	int * virtScreenH;
	int * cameraX;
	int * cameraY;
};
extern globalGraphicsContext_t globalGraphicsContext;

class GraphicsContext_c
{
	public:
	// ---- values
	// OpenGL
	GLuint program;
	GLint           mv_location;
	GLint           proj_location;
	GLint           col1;
	float           aspect;
	vmath::mat4     proj_matrix;

	GLuint fs;
	GLuint vs;
	// GLFW
	GLFWwindow * glfwWindow;
	// -- other
	int windowW;
	int windowH;
	int virtScreenW;
	int virtScreenH;
	int cameraX;
	int cameraY;

	GLfloat backgroundColor[4];
	GLfloat depthValue = 1.0f; //figure out what this is TODO
	
	std::string vs_source_s;
	std::string fs_source_s;

	// ---- functions
	GraphicsContext_c();
	~GraphicsContext_c();
	void setupGLFWRenderCallbackPointers(GraphicsContext_c &Render);
	void render();
};

void resizeWindow(GLFWwindow* window, int width, int height);

#endif

