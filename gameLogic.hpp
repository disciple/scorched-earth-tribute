#ifndef GAME_LOGIC_HPP
#define GAME_LOGIC_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "input.hpp"
#include "graphicsContext.hpp"
#include "entities.hpp"
#include <iostream>
const double TIME_SLICE = 0.016;

enum gameState_e
{
	PLAYER_TURN,
	WEAPON_EFFECTS,
	LAND_FALL,
	TANK_FALL
};

class GameLogic_c
{
	public:
	// ---- values
	gameState_e gameState;
	int targetVirtPixX;
	int targetVirtPixY;
	double virtCursorX;
	double virtCursorY;
	Input_c Input;
	// ---- functions
	GameLogic_c();
	~GameLogic_c();
	void update();
	void updateTargetVirtPix();
	void processInput();
};
#endif
