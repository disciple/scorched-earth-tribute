#ifndef ENTITIES_HPP
#define ENTITIES_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "utility.hpp"
#include "cmath"
#include <iostream>
#include <vector>
#define MAX_WEAPON_COUNT 1
#define TANK_GUN_LENGTH 4.0
#define TANK_GUN_Y_DISP 2.5
enum weaponType_e
{	
	STANDARD_ROUND
};
// can the projectile be deleted?
enum completionStatus_e
{
	NOT_DONE,
	DONE
};
class Projectile_c
{
	public:
	// ---- values
	weaponType_e weaponType;
	GLfloat xOrigin;
	GLfloat yOrigin;
	GLfloat xLoc;
	GLfloat yLoc;
	GLfloat xVelocity;
	GLfloat yVelocity;
	// ---- functions
	Projectile_c();
	~Projectile_c();
	void advance(double timeDisp);
	completionStatus_e checkForEvents();
	void conventionalExplosion(int xPix, int yPix, int radius);
};

enum tankType_e
{
	STANDARD
};

class Tank_c
{
	public:
	// ---- values
	bool traction; // signifies if the tank can drive
	tankType_e tankType; 
	int health;
	weaponType_e currentWeapon;
	int weapons[MAX_WEAPON_COUNT];
	std::vector<Projectile_c> Projectiles;
	// gun rotation stored in degrees
	GLfloat gunRotation;
	GLfloat gunHeight;
	float maxPower;
	// worlds coords the hit detection/final position is based off of. Center of tank
	int x;
	int y;
	// more accurate world coords to be used for more accurate falling/position
	GLfloat accuX;
	GLfloat accuY;
	GLfloat accuZ;
	// ---- functions
	Tank_c();
	~Tank_c();
	void updateGunRotation(double virtCursorX, double virtCursorY);
	// modifies the accurate coords and updates the int coords
	void displaceCoords(double xDisp, double yDisp);
	void setCoords(double x, double y);
	void fireProjectile(weaponType_e weaponType);
	void updateProjectileStatus(double timeDisp);
};

class Entities_c
{
	public:
	// ---- values
	// virtual pixels
	int virtualPixelCountW;
	int virtualPixelCountH;
	int virtualPixelCount;
	bool * virtualPixels;
	bool virtPixDropComplete;
	
	// Projectile
	std::vector<Projectile_c> Projectiles;

	// Tank 
	bool allTanksHaveTraction;
	std::vector<Tank_c>	Tanks;
	
	// ---- functions
	Entities_c();
	~Entities_c();
	void applyVirtCursor(double virtCursorX, double virtCursorY);
	void updateState_Time(double timeDisp);
	// virtual pixels
	int getColumnHeight(int index);
	enum mutateType_e
	{
		ADD,
		REMOVE
	};
	void mutateDirt(int x, int y, int radius, mutateType_e mutateType);
	enum dirt_e
	{
		ADD_DIRT,
		REMOVE_DIRT
	};
	void dropVirtPixels(double time);

	// entity/Virtual pixel interaction
	void dropTanks(double timeDisp);
	void advanceProjectiles(double timeDisp);
	void updateProjectileStatuses(double timeDisp);
	void updateTanks(double timeDisp);
};
extern Entities_c * EntitiesPointer;

#endif
