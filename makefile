OBJS = main.cpp graphics.cpp graphicsContext.cpp scorch.cpp entities.cpp gameLogic.cpp input.cpp utility.cpp

OBJ_NAME = Scorch

#INCLUDE_PATH gives the directory of header files
INCLUDE_PATH = -I"C:\MinGW\ExternLib-GLFW\include"
INCLUDE_PATH += -I"C:\MinGW\ExternLib-GLM"
INCLUDE_PATH += -I"C:\MinGW\ExternLib-Glew\include"
INCLUDE_PATH += -I"C:\MinGW\ExternLib-ENET\include"

#LIBRARY_PATH gives the directory of lib files
LIBRARY_PATH = -L"C:\MinGW\ExternLib-Glew\lib"
LIBRARY_PATH += -L"C:\MinGW\ExternLib-GLFW\lib"
INCLUDE_PATH += -L"C:\MinGW\ExternLib-ENET\lib"

#--------------------------------------------------LINKER
NETWORK_FLAGS = -lenet -lws2_32 -lwinmm
WINDOWS_LIBS = -mconsole -lgdi32

#LINKER_FLAG specifies various libraries and flags
#-static flag to avoid needing to include a pthread .dll for c++11 features
#LINKER_FLAG = -std=c++11 -lglfw3 -lopengl32 -lglu32 -lmingw32 $(WINDOWS_LIBS) -static-libgcc -static-libstdc++ $(NETWORK_FLAGS) -static
WINDOWS_LIBS = -mconsole -lgdi32
#To not use a terminal:
#WINDOWS_LIBS = -mwindows
LINKER_FLAG = -lglfw3 -lglew32 -lopengl32 -lglu32 -std=gnu++11 $(WINDOWS_LIBS) $(NETWORK_FLAGS)

#This is the target that compiles our executable
all : $(OBJS)
	g++  -o $(OBJ_NAME) $(OBJS) $(INCLUDE_PATH) $(LIBRARY_PATH) $(LINKER_FLAG)
