#include "graphicsContext.hpp"
globalGraphicsContext_t globalGraphicsContext;
// -------- START CLASS RENDER
GraphicsContext_c::GraphicsContext_c():
	windowW(1366),
	windowH(768),
	virtScreenW(720),
	virtScreenH(405),
	cameraX(0),
	cameraY(0)
{
	backgroundColor[0] = 0.5f;
	backgroundColor[1] = 0.5f;
	backgroundColor[2] = 0.5f;
	backgroundColor[3] = 1.0f;
//	depthValue = 1.0f;

	std::cout<<"Initializing GraphicsContext_c\n";
	// ---- set the global globalGraphicsContext to point to Render's variables
	//making ** point to pointer of pointer value in class
	globalGraphicsContext.glfwWindow = &glfwWindow;
	globalGraphicsContext.windowW = &windowW;
	globalGraphicsContext.windowH = &windowH;
	globalGraphicsContext.virtScreenW = &virtScreenW;
	globalGraphicsContext.virtScreenH = &virtScreenH;
	globalGraphicsContext.cameraX = &cameraX;
	globalGraphicsContext.cameraY = &cameraY;
	// ---- END set
	
	glfwInit();
	glfwWindowHint( GLFW_CONTEXT_VERSION_MAJOR, 4 );
	glfwWindowHint( GLFW_CONTEXT_VERSION_MINOR, 1 );
	glfwWindowHint( GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE );
	//TODO revert to using windowW/H
	glfwWindow = glfwCreateWindow(windowW, windowH, "Scorch", NULL, NULL);
	glfwMakeContextCurrent(glfwWindow);

	// set GLFW callbacks
	glfwSetFramebufferSizeCallback(glfwWindow, resizeWindow);

	glewExperimental = GL_TRUE;
	glewInit();
	static const char * vs_source[] =
	{
			"#version 410 core                                                  \n"
			"                                                                   \n"
			"in vec4 position;                                                  \n"
			"                                                                   \n"
			"out VS_OUT                                                         \n"
			"{                                                                  \n"
			"    vec4 color;                                                    \n"
			"} vs_out;                                                          \n"
			"                                                                   \n"
			"uniform mat4 mv_matrix;                                            \n"
			"uniform mat4 proj_matrix;                                          \n"
			"uniform vec4 co;                                                   \n"
			"                                                                   \n"
			"void main(void)                                                    \n"
			"{                                                                  \n"
			"    gl_Position = proj_matrix * mv_matrix * position;              \n"
			// "    vs_out.color = vec4(1.0, 0.0, 0.0, 1.0);                       \n"
			"    vs_out.color = position * 0.1 + vec4(0.3, 0.2, 0.5, 1.0);      \n"
			// "    vs_out.color = co;                                             \n"
			"}                                                                  \n"
	};

	static const char * fs_source[] =
	{
			"#version 410 core                                                  \n"
			"                                                                   \n"
			"out vec4 color;                                                    \n"
			"                                                                   \n"
			"in VS_OUT                                                          \n"
			"{                                                                  \n"
			"    vec4 color;                                                    \n"
			"} fs_in;                                                           \n"
			"                                                                   \n"
			"void main(void)                                                    \n"
			"{                                                                  \n"
			"    color = fs_in.color;                                           \n"
			"}                                                                  \n"
	};

	/*TODO dynamic shader loading
	vs_source_s = vs_source[0];
	fs_source_s = fs_source[0];
	*/

	program = glCreateProgram();
	fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, fs_source, NULL);
	glCompileShader(fs);
	vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, vs_source, NULL);
	glCompileShader(vs);
	glAttachShader(program, vs);
	glAttachShader(program, fs);
	glLinkProgram(program);
	mv_location = glGetUniformLocation(program, "mv_matrix");
	proj_location = glGetUniformLocation(program, "proj_matrix");
	col1 = glGetUniformLocation(program, "co");

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LEQUAL);
	glViewport(0, 0, windowW, windowH);

	glClearBufferfv(GL_COLOR, 0, backgroundColor);
	glClearBufferfv(GL_DEPTH, 0, &depthValue);
}

GraphicsContext_c::~GraphicsContext_c()
{
	std::cout<<"Deleting GraphicsContext_c\n";
}

// -------- END CLASS RENDER

//callback
void resizeWindow(GLFWwindow* window, int width, int height)
{
	*globalGraphicsContext.windowW = width;
	*globalGraphicsContext.windowH = height;
	glViewport(0, 0, width, height);
	std::cout<<"window changed to ("<<*globalGraphicsContext.windowW<<
		", "<<*globalGraphicsContext.windowH<<")\n";
}
