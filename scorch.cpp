#include "scorch.hpp"
Scorch_c::Scorch_c()
{
	std::cout<<"Initializing Scorch_c\n";
	// -- assign pointers and do any pointer related post-initialization
	// connect Graphics class to Entities to render all objects based off of game data
	Graphics.Entities = &Entities;
}

Scorch_c::~Scorch_c()
{
	std::cout<<"Deleting Scorch_c\n";
}

void Scorch_c::mainLoop()
{
	while(!Graphics.closeWindow())
	{
		GameLogic.update();
		Graphics.render();
		glfwPollEvents();
	}
}
