#ifndef GRAPHICS_CONTEXT_HPP
#define GRAPHICS_CONTEXT_HPP
#include "GL/glew.h"
#include "GLFW/glfw3.h"
#include "utility.hpp"
#include "vmath.h"
#include "entities.hpp"
#include "graphicsContext.hpp"
#include <vector>

// dependent on EntitiesPointer global
class EnvironmentGeo_c
{
	public:
	// ---- values
	GLuint vao;
	GLuint buffer;
	GLuint indexBufferID;
	int gridW;
	int gridH;
	int vertexGridCount;
	int triangleCount;
	vectorf_t * grid;
	GLuint * indices;	
	// ---- functions
	EnvironmentGeo_c();
	~EnvironmentGeo_c();
	void setVertexGrid();
	void setLandscapeIndices();
};

class ProjectileGeo_c
{
	public:
	// ---- values
	// index for list of type ProjectileGeo_c to separate vector of projectile geomoetry
	int typeIndex;
	int projectileTriangleCount;
	GLuint projectileVAO;
	GLuint projectileBuffer;
	GLuint projectileIndexBufferID;
	vectorf_t * projectileVertices;
	GLuint * projectileIndices;
	// ---- functions
	ProjectileGeo_c(int givenProjectileIndex);
	~ProjectileGeo_c();
	void setProjectileVertices();
	void setProjectileIndices();
};

class TankGeo_c
{
	public:
	// ---- values
	// - tank body
	GLuint tankVAO;
	GLuint tankBuffer;
	GLuint tankIndexBufferID;
	// - tank gun
	GLuint tankGunVAO;
	GLuint tankGunBuffer;
	GLuint tankGunIndexBufferID;

	int tankTypeIndex;
	int tankTriangleCount;
	vectorf_t * tankVertices;
	GLuint * tankIndices;
	int tankGunTriangleCount;
	vectorf_t * tankGunVertices;
	GLuint * tankGunIndices;
	// ---- functions
	TankGeo_c();
	~TankGeo_c();
	void setTankVertices();
	void setTankIndices();
	void setTankGunVertices();
	void setTankGunIndices();
};

class Graphics_c
{
	public:
	// ---- values
	GraphicsContext_c GraphicsContext;
	// ---- graphics data
	TankGeo_c TankGeo;
	ProjectileGeo_c ProjectileGeo;
	EnvironmentGeo_c EnvironmentGeo;
	std::vector<TankGeo_c> TankGeos;
	// -- other
	Entities_c * Entities;
	// ---- functions
	Graphics_c();
	~Graphics_c();
	void render();
	void renderEnvironment();
	void renderTanks();
	void renderProjectiles();
	bool closeWindow();
};

#endif
