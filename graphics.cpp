#include "graphics.hpp"
// -------- START CLASS LAND_GEO
EnvironmentGeo_c::EnvironmentGeo_c()
{
	std::cout<<"Initializing EnvironmentGeo_c\n";
	// connect the EntitiesPointer data from Entities_c to EnvironmentGeo's EntitiesPointer pointer
	// general data
	gridW = EntitiesPointer->virtualPixelCountW + 1;
	gridH = EntitiesPointer->virtualPixelCountH + 1;
	vertexGridCount = gridW * gridH;
	triangleCount = EntitiesPointer->virtualPixelCount * 2;
	grid = new vectorf_t[vertexGridCount];
	setVertexGrid();
	int indicesPerVirtPix = 6;
	indices = new GLuint[EntitiesPointer->virtualPixelCount * indicesPerVirtPix];
	setLandscapeIndices();
	// OpenGL 
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBufferData(GL_ARRAY_BUFFER, vertexGridCount * sizeof(vectorf_t), grid, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glGenBuffers(1,&indexBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, EntitiesPointer->virtualPixelCount * sizeof(GLuint)
		   	    * indicesPerVirtPix, indices, GL_DYNAMIC_DRAW);
}

EnvironmentGeo_c::~EnvironmentGeo_c()
{
	std::cout<<"Deleting EnvironmentGeo_c\n";
	delete[] grid;
	delete[] indices;
}

void EnvironmentGeo_c::setVertexGrid()
{
	for(int i = 0; i < gridH; ++i)
	{
		for(int j = 0; j < gridW; ++j)
		{
			grid[(i * gridW) + j].x = (GLfloat) j;
			grid[(i * gridW) + j].y = (GLfloat) i;
			grid[(i * gridW) + j].z = 0.0f;
		}
	}
}

void EnvironmentGeo_c::setLandscapeIndices()
{
	triangleCount = 0;
	for(int i=0; i < EntitiesPointer->virtualPixelCount; ++i)
	{
		if(EntitiesPointer->virtualPixels[i])
		{
			int y = ((int) i / (int) EntitiesPointer->virtualPixelCountW);
			//first triangle
			indices[triangleCount*3 + 0] = i + y;
			indices[triangleCount*3 + 1] = i + y + 1;
			indices[triangleCount*3 + 2] = i + y + 1 + EntitiesPointer->virtualPixelCountW;
			++triangleCount;
			//second triangle
			indices[triangleCount*3 + 0] = i + y + 2 + EntitiesPointer->virtualPixelCountW;
			indices[triangleCount*3 + 1] = i + y + 1 + EntitiesPointer->virtualPixelCountW;
			indices[triangleCount*3 + 2] = i + y + 1;
			++triangleCount;
		}
	}
}
// -------- END CLASS LAND_GEO

// -------- START CLASS PROJECTILE_GEO
ProjectileGeo_c::ProjectileGeo_c(int givenProjectileIndex):
	typeIndex(givenProjectileIndex)
{
	std::cout<<"Initializing ProjectileGeo_c with projectileTypeIndex = "<<typeIndex<<"\n";
	setProjectileVertices();
	setProjectileIndices();
	glGenVertexArrays(1, &projectileVAO);
	glBindVertexArray(projectileVAO);
	glGenBuffers(1, &projectileBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, projectileBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vectorf_t) * projectileTriangleCount * 3, projectileVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &projectileIndexBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, projectileIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * projectileTriangleCount * 3, projectileIndices, GL_DYNAMIC_DRAW);
}

ProjectileGeo_c::~ProjectileGeo_c()
{
	std::cout<<"Deleting ProjectileGeo_c\n";
	delete[] projectileVertices;
	delete[] projectileIndices;
}

void ProjectileGeo_c::setProjectileVertices()
{
	GLfloat z = 1.0;
	GLfloat y = 0;
	projectileVertices = new vectorf_t[14];
	projectileVertices[0] = {-3.0f, y, z};
	projectileVertices[1] = {3.0f, y, z};
	y += 1;
	projectileVertices[2] = {-4.0f, y, z};
	projectileVertices[3] = {-3.0f, y, z};
	projectileVertices[4] = {3.0f, y, z};
	projectileVertices[5] = {4.0f, y, z};
	y += 1;
	projectileVertices[6] = {-4.0f, y, z};
	projectileVertices[7] = {-3.0f, y, z};
	projectileVertices[8] = {-2.0f, y, z};
	projectileVertices[9] = {2.0f, y, z};
	projectileVertices[10] = {3.0f, y, z};
	projectileVertices[11] = {4.0f, y, z};
	y += 1;
	projectileVertices[12] = {-2.0f, y, z};
	projectileVertices[13] = {2.0f, y, z};
}

void ProjectileGeo_c::setProjectileIndices()
{
	projectileTriangleCount = 0;
	int indexCount = 30;
	projectileIndices = new GLuint[indexCount];
	//bottom chunk
	projectileIndices[0] = 0;
	projectileIndices[1] = 3;
	projectileIndices[2] = 2;
	projectileIndices[3] = 0;
	projectileIndices[4] = 4;
	projectileIndices[5] = 3;
	projectileIndices[6] = 0;
	projectileIndices[7] = 1;
	projectileIndices[8] = 4;
	projectileIndices[9] = 1;
	projectileIndices[10] = 5;
	projectileIndices[11] = 4;
	//middle chunk
	projectileIndices[12] = 2;
	projectileIndices[13] = 11;
	projectileIndices[14] = 6;
	projectileIndices[15] = 2;
	projectileIndices[16] = 5;
	projectileIndices[17] = 11;
	//top chunk
	projectileIndices[18] = 7;
	projectileIndices[19] = 8;
	projectileIndices[20] = 12;
	projectileIndices[21] = 8;
	projectileIndices[22] = 13;
	projectileIndices[23] = 12;
	projectileIndices[24] = 8;
	projectileIndices[25] = 9;
	projectileIndices[26] = 13;
	projectileIndices[27] = 9;
	projectileIndices[28] = 10;
	projectileIndices[29] = 13;

	projectileTriangleCount = indexCount / 3;
}

// -------- END CLASS PROJECTILE_GEO

// -------- START CLASS TANK_GEO
TankGeo_c::TankGeo_c()
{
	std::cout<<"Initializing TankGeo_c\n";
	// -- tank body
	setTankVertices();
	setTankIndices();
	glGenVertexArrays(1, &tankVAO);
	glBindVertexArray(tankVAO);
	glGenBuffers(1, &tankBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, tankBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vectorf_t) * tankTriangleCount * 3, tankVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);

	glGenBuffers(1, &tankIndexBufferID);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tankIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * tankTriangleCount * 3, tankIndices, GL_DYNAMIC_DRAW);
	// -- tank gun
	setTankGunVertices();
	setTankGunIndices();
	glGenVertexArrays(1, &tankGunVAO);
	glBindVertexArray(tankGunVAO);
	glGenBuffers(1, &tankGunBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, tankGunBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vectorf_t) * tankGunTriangleCount * 3,
		   	tankGunVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(0);
	glGenBuffers(1,&tankGunIndexBufferID);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tankGunIndexBufferID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(GLuint) * tankGunTriangleCount * 3, tankGunIndices, GL_DYNAMIC_DRAW);
}

TankGeo_c::~TankGeo_c()
{
	std::cout<<"Deleting TankGeo_c\n";
	delete[] tankVertices;
	delete[] tankIndices;
	delete[] tankGunVertices;
	delete[] tankGunIndices;
}

void TankGeo_c::setTankVertices()
{
	GLfloat z = 1.0;
	GLfloat y = 0;
	tankVertices = new vectorf_t[14];
	tankVertices[0] = {-3.0f, y, z};
	tankVertices[1] = {3.0f, y, z};
	y += 1;
	tankVertices[2] = {-4.0f, y, z};
	tankVertices[3] = {-3.0f, y, z};
	tankVertices[4] = {3.0f, y, z};
	tankVertices[5] = {4.0f, y, z};
	y += 1;
	tankVertices[6] = {-4.0f, y, z};
	tankVertices[7] = {-3.0f, y, z};
	tankVertices[8] = {-2.0f, y, z};
	tankVertices[9] = {2.0f, y, z};
	tankVertices[10] = {3.0f, y, z};
	tankVertices[11] = {4.0f, y, z};
	y += 1;
	tankVertices[12] = {-2.0f, y, z};
	tankVertices[13] = {2.0f, y, z};
}

void TankGeo_c::setTankIndices()
{
	tankTriangleCount = 0;
	int indexCount = 30;
	tankIndices = new GLuint[indexCount];
	//bottom chunk
	tankIndices[0] = 0;
	tankIndices[1] = 3;
	tankIndices[2] = 2;
	tankIndices[3] = 0;
	tankIndices[4] = 4;
	tankIndices[5] = 3;
	tankIndices[6] = 0;
	tankIndices[7] = 1;
	tankIndices[8] = 4;
	tankIndices[9] = 1;
	tankIndices[10] = 5;
	tankIndices[11] = 4;
	//middle chunk
	tankIndices[12] = 2;
	tankIndices[13] = 11;
	tankIndices[14] = 6;
	tankIndices[15] = 2;
	tankIndices[16] = 5;
	tankIndices[17] = 11;
	//top chunk
	tankIndices[18] = 7;
	tankIndices[19] = 8;
	tankIndices[20] = 12;
	tankIndices[21] = 8;
	tankIndices[22] = 13;
	tankIndices[23] = 12;
	tankIndices[24] = 8;
	tankIndices[25] = 9;
	tankIndices[26] = 13;
	tankIndices[27] = 9;
	tankIndices[28] = 10;
	tankIndices[29] = 13;

	tankTriangleCount = indexCount / 3;
}

void TankGeo_c::setTankGunVertices()
{
	GLfloat z = 0.9;
	GLfloat y = -0.6;
	GLfloat gunLength = 6.0f;
	tankGunVertices = new vectorf_t[4];
	tankGunVertices[0] = {0.0f, y, z};
	tankGunVertices[1] = {gunLength, y, z};
	y *= -1;
	tankGunVertices[2] = {0.0f, y, z};
	tankGunVertices[3] = {gunLength, y, z};
}

void TankGeo_c::setTankGunIndices()
{
	tankGunTriangleCount = 0;
	int indexCount = 6;
	tankGunIndices = new GLuint[indexCount];
	tankGunIndices[0] = 0;
	tankGunIndices[1] = 1;
	tankGunIndices[2] = 3;
	tankGunIndices[3] = 0;
	tankGunIndices[4] = 3;
	tankGunIndices[5] = 2;
	tankGunTriangleCount = indexCount / 3;
}
// -------- END CLASS TANK_GEO

// -------- START CLASS GRAPHICS
Graphics_c::Graphics_c():
	ProjectileGeo(0)
{
	std::cout<<"Initializing Graphics_c\n";
}

Graphics_c::~Graphics_c()
{
	std::cout<<"Deleting Graphics_c\n";
}

void Graphics_c::render()
{
	GraphicsContext.proj_matrix = vmath::ortho(0.0f, GraphicsContext.virtScreenW, 0.0f, GraphicsContext.virtScreenH, -10.0f, 10.0f);
	
	// clear buffers
	glClearBufferfv(GL_COLOR, 0, GraphicsContext.backgroundColor);
	glClearBufferfv(GL_DEPTH, 0, &GraphicsContext.depthValue);
	// use base shader
	glUseProgram(GraphicsContext.program);
	glUniformMatrix4fv(GraphicsContext.proj_location, 1, GL_FALSE, GraphicsContext.proj_matrix);

	//update landscape appearance
	EnvironmentGeo.setLandscapeIndices();

	renderEnvironment();
	renderTanks();
	renderProjectiles();

	// flush OpenGL commands
	glfwSwapBuffers(GraphicsContext.glfwWindow);
}

void Graphics_c::renderEnvironment()
{
	glBindVertexArray(EnvironmentGeo.vao);
	glUniformMatrix4fv(GraphicsContext.proj_location, 1, GL_FALSE, GraphicsContext.proj_matrix);
	//update indices
	glBufferSubData(GL_ELEMENT_ARRAY_BUFFER, 0, EnvironmentGeo.triangleCount * sizeof(GLuint) * 3, EnvironmentGeo.indices);
	vmath::mat4 mv_matrix = vmath::translate(0.0f, 0.0f, 0.0f);
	glUniformMatrix4fv(GraphicsContext.mv_location, 1, GL_FALSE, mv_matrix);
	glDrawElements(GL_TRIANGLES, EnvironmentGeo.triangleCount * 3, GL_UNSIGNED_INT, 0);
}

// TODO with render functions instead of using instance of xGeo, create vectors so different models can be used
void Graphics_c::renderTanks()
{
	// render tank body
	glBindVertexArray(TankGeo.tankVAO);
	glUniformMatrix4fv(GraphicsContext.proj_location, 1, GL_FALSE, GraphicsContext.proj_matrix);
	vmath::mat4 mv_matrix = vmath::translate(0.0f, 0.0f, 0.0f);
	for(int i = 0; i < Entities->Tanks.size(); ++i)
	{
		mv_matrix = vmath::translate(Entities->Tanks[i].accuX, Entities->Tanks[i].accuY, 0.0f);
		glUniformMatrix4fv(GraphicsContext.mv_location, 1, GL_FALSE, mv_matrix);
		glDrawElements(GL_TRIANGLES, TankGeo.tankTriangleCount * 9, GL_UNSIGNED_INT, 0);
	}
	// render tank gun
	glBindVertexArray(TankGeo.tankGunVAO);
	glUniformMatrix4fv(GraphicsContext.proj_location, 1, GL_FALSE, GraphicsContext.proj_matrix);
	for(int i = 0; i < Entities->Tanks.size(); ++i)
	{
		GLfloat gunHeight = TANK_GUN_Y_DISP;
		mv_matrix = vmath::translate(Entities->Tanks[i].accuX, Entities->Tanks[i].accuY + 
				Entities->Tanks[i].gunHeight, 0.0f);//creates identity

		//TODO implement nicer storage of z axis after POC
		vmath::vec3 zaxis(0.0f, 0.0f, Entities->Tanks[i].accuZ);
	   	mv_matrix *= vmath::rotate(Entities->Tanks[i].gunRotation, zaxis);

		glUniformMatrix4fv(GraphicsContext.mv_location, 1, GL_FALSE, mv_matrix);
		glDrawElements(GL_TRIANGLES, TankGeo.tankGunTriangleCount * 3, GL_UNSIGNED_INT, 0);
	}
}

void Graphics_c::renderProjectiles()
{
	vmath::mat4 mv_matrix = vmath::translate(0.0f, 0.0f, 0.0f);
	int tankCount = Entities->Tanks.size();
	for(int i = 0; i < tankCount; ++i)
	{
		int projectileCount = Entities->Tanks[i].Projectiles.size();
		for(int j = 0; j < projectileCount; ++j)
		{
			glBindVertexArray(ProjectileGeo.projectileVAO);
			mv_matrix = vmath::translate(Entities->Tanks[i].Projectiles[j].xLoc
					, Entities->Tanks[i].Projectiles[j].yLoc, 0.0f);
			glUniformMatrix4fv(GraphicsContext.mv_location, 1, GL_FALSE, mv_matrix);
			glDrawElements(GL_TRIANGLES, ProjectileGeo.projectileTriangleCount * 9, GL_UNSIGNED_INT, 0);
		}	
	}
}

bool Graphics_c::closeWindow()
{
	return glfwWindowShouldClose(GraphicsContext.glfwWindow);
}
// -------- END CLASS GRAPHICS
